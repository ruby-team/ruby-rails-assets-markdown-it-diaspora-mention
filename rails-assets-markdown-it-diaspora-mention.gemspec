# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rails-assets-markdown-it-diaspora-mention/version'

Gem::Specification.new do |spec|
  spec.name          = "rails-assets-markdown-it-diaspora-mention"
  spec.version       = RailsAssetsMarkdownItDiasporaMention::VERSION
  spec.authors       = ["rails-assets.org"]
  spec.description   = "diaspora* mentions for markdown-it markdown parser"
  spec.summary       = "diaspora* mentions for markdown-it markdown parser"
  spec.homepage      = "https://github.com/diaspora/markdown-it-diaspora-mention"
  spec.license       = "MIT"

  spec.files         = `find ./* -type f | cut -b 3-`.split($/)
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
